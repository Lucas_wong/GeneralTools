# -*- coding: utf-8 -*-
"""
-----------------------------------------------------
    File Name:        ReadExcel
    Author:           Lucas.wang
    Date:             2018-12-18 11:45
    Description:      
-----------------------------------------------------
    Change Activity:  2018-12-18 11:45
    Description:      
----------------------------------------------------
"""
import xlrd
import Oracle.cxOracle as oracle
import sys

import importlib
importlib.reload(sys)

def open_excel(file= 'IC-854.xlsx'):
    try:
        data = xlrd.open_workbook(file)
        return data
    except Exception as ex:
        print(str(ex))
#根据索引获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的所以  ，by_index：表的索引
def excel_table_byindex(file= 'IC-854.xlsx',colnameindex=0,by_index=0):
    data = open_excel(file)
    table = data.sheets()[by_index]
    nrows = table.nrows #行数
    ncols = table.ncols #列数
    colnames =  table.row_values(colnameindex) #某一行数据
    list =[]
    for rownum in range(1,nrows):

         row = table.row_values(rownum)
         if row:
             app = {}
             for i in range(len(colnames)):
                app[colnames[i]] = row[i]
             list.append(app)
    return list

#根据名称获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的所以  ，by_name：Sheet1名称
def excel_table_byname(file= 'file.xls',colnameindex=0,by_name=u'Sheet1'):
    data = open_excel(file)
    table = data.sheet_by_name(by_name)
    nrows = table.nrows #行数
    colnames =  table.row_values(colnameindex) #某一行数据
    list =[]
    for rownum in range(1,nrows):
         row = table.row_values(rownum)
         if row:
             app = {}
             for i in range(len(colnames)):
                app[colnames[i]] = row[i]
             list.append(app)
    return list

def update_DB(tables):

    tns = r'(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=iconndbdev.ariix.com)(PORT=1521)))(CONNECT_DATA=(SID=iconndev)))'
    ora = oracle.cxOracle('iconn', 'iconn', tns)

    for row in tables:
        # print(row)

        if row['TABLE NAME'] == "CNST_CUST_SEARCH_RULES":
            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','zh-CN','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Chinese Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','zh-TW','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Chinese Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','ja-JP','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Japanese Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','ko-KR','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Korean Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','nl-NL','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Dutch Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','fr-FR','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['French Values'])
            print(sql)
            # ora.Exec(sql)

            sql = "INSERT INTO CONST_I18N (TABLE_NAME,LANGUAGE_ID,KEY,VAL) VALUES ('%s','ru-RU','%s','%s');" % (row['TABLE NAME'], int(row['KEY']) if is_number(row['KEY']) else str(row['KEY']), row['Russian Values'])
            print(sql)
            # ora.Exec(sql)

def is_number(s):
    try:  # 如果能运行float(s)语句，返回True（字符串s是浮点数）
        float(s)
        return True
    except ValueError:  # ValueError为Python的一种标准异常，表示"传入无效的参数"
        pass  # 如果引发了ValueError这种异常，不做任何事情（pass：不做任何事情，一般用做占位语句）
    try:
        import unicodedata  # 处理ASCii码的包
        # unicodedata.numeric(s)  # 把一个表示数字的字符串转换为浮点数返回的函数
        unicodedata.digit(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

def main():
   tables = excel_table_byindex(file= 'IC-854.xlsx')
   # for row in tables:
   #     print(row)
   update_DB(tables)

   # tables = excel_table_byname(file= 'IC-854.xlsx',by_name=u'CONST-I18N')
   # for row in tables:
   #     print(row)

if __name__=="__main__":
    main()