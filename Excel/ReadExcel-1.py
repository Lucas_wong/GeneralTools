# -*- coding: utf-8 -*-
"""
-----------------------------------------------------
    File Name:        ReadExcel
    Author:           Lucas.wang
    Date:             2018-12-18 10:58
    Description:      
-----------------------------------------------------
    Change Activity:  2018-12-18 10:58
    Description:      
----------------------------------------------------
"""
import xlrd
from datetime import date, datetime

def read_excel():
    #文件位置

    ExcelFile=xlrd.open_workbook(r'IC-854.xlsx')

    #获取目标EXCEL文件sheet名

    print(ExcelFile.sheet_names())

    #------------------------------------

    #若有多个sheet，则需要指定读取目标sheet例如读取sheet2

    #sheet2_name=ExcelFile.sheet_names()[1]

    #------------------------------------

    #获取sheet内容【1.根据sheet索引2.根据sheet名称】

    #sheet=ExcelFile.sheet_by_index(1)

    sheet=ExcelFile.sheet_by_name('CONST-I18N')

    #打印sheet的名称，行数，列数

    print(sheet.name, sheet.nrows, sheet.ncols)

    #获取整行或者整列的值

    rows = sheet.row_values(3)#第三行内容

    cols = sheet.col_values(1)#第二列内容

    print(cols)
    print(rows)

    #获取单元格内容

    print(sheet.cell(3, 3).value.encode('utf-8'))

    print(sheet.cell_value(2, 0).encode('utf-8'))

    print(sheet.row(3)[3].value.encode('utf-8'))

    print(sheet.row(3)[3].value)

    #打印单元格内容格式
    # ctype介绍 :
    # 0empty    1string     2number    3date    4boolean    5error

    print(sheet.cell(3, 2).ctype)

if __name__ =='__main__':

    read_excel()